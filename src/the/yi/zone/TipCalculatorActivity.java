package the.yi.zone;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.Window;

public class TipCalculatorActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        Button tenPercent = (Button)findViewById(R.id.button1);
        Button fifteenPercent = (Button)findViewById(R.id.button2);
        Button twentyPercent = (Button)findViewById(R.id.button3);
        tenPercent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String num1 = ((EditText)findViewById(R.id.editText1)).getText().toString();
				if (num1.equals("")){
				   num1 = "0";
				}
				double n1 = Double.parseDouble(num1);
				double result = n1 * .10;
				TextView res = (TextView)findViewById(R.id.textView2);
				res.setText("You tip: "+Double.toString(result) + "$");
			}}
		);
        fifteenPercent.setOnClickListener(new View.OnClickListener(){
        	public void onClick (View v){
        		String num1 = ((EditText)findViewById(R.id.editText1)).getText().toString();
        		if (num1.equals("")){
 				   num1 = "0";
 				}
        		double n1 = Double.parseDouble(num1);
        		double result = n1* .15;
        		TextView res = (TextView)findViewById(R.id.textView2);
        		res.setText("You tip: "+Double.toString(result) + "$");
        	}
        });
        twentyPercent.setOnClickListener(new View.OnClickListener(){
        	public void onClick (View v){
        		String num1 = ((EditText)findViewById(R.id.editText1)).getText().toString();
        		if (num1.equals("")){
 				   num1 = "0";
 				}
        		double n1 = Double.parseDouble(num1);
        		double result = n1* .20;
        		TextView res = (TextView)findViewById(R.id.textView2);
        		res.setText("You tip: "+Double.toString(result) + "$");
        	}
        });	
    }

    	
    }
    
